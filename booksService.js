const { Book } = require('./models/Books.js');

function createBook(req, res, next) {
  try{
    const { text, userId } = req.body;
    const book = new Book({
      "userId": req.user.userId,
      "text": req.body.text,
    });
    book.save().then((saved) => {
          res.status(200).send({
            "message": "Success"
        })
    }).catch((err)=>{
      console.log(err)
      return res.status(400).json({"message": "string"});
    });
  }
  catch(err){
    return res.status(500).json({"message": "string"});
  }

}

async function getBooks(req, res, next) {
  try{
    return Book.find({'userId' : req.user.userId}).select('userId completed text createdDate')
      .then((result) => {
      // console.log('result', result)
      res.status(200).send({
        "offset": req.body.offset,
        "limit": req.body.limit,
        "count": 0,
        "notes": result
      })
    }).catch((err)=>{
      console.log('err', err)
      return res.status(400).json({"message": "string"});
    })
  }
  catch(err){
    return res.status(500).json({"message": "string"});
  }
}

const getBook = (req, res, next) => {
  try{
    Book.findById(req.params.id).select('userId completed text createdDate')
    .then((book) => {
      res.status(200).send({
        "note": book
      })
    }).catch((err)=>{
      console.log('err', err)
      return res.status(400).json({"message": "string"});
    })
  }
  catch(err){
    return res.status(500).json({"message": "string"});
  }
}

const updateBook = async (req, res, next) => {
  const book = await Book.findById(req.params.id);
  const { title, author } = req.body;

  if (title) book.title = title;
  if (author) book.author = author;

  return book.save().then((saved) => res.json(saved));
};

const deleteBook = (req, res, next) => {
  try{
      Book.findByIdAndDelete(req.params.id)
    .then((book) => {
      res.status(200).send({
        "message": "Success"
      })
    });
  }
  catch(err){
    return res.status(500).json({"message": "string"});
  }
}
const getMyBooks = (req, res, next) => {
  return Book.find({userId: req.user.userId}, '-__v').then((result) => {
    res.json(result);
  });
}

const updateMyBookById = (req, res, next) => {
  try{
    const { text } = req.body;
    return Book.findByIdAndUpdate({_id: req.params.id, userId: req.user.userId}, {$set: { text } })
      .then((result) => {
        res.status(200).send({
          "message": "Success"
        })
      }).catch((err)=>{
        console.log('err', err)
        return res.status(400).json({"message": "string"});
      })
  }
  catch(err){
    return res.status(500).json({"message": "string"});
  }
  
}

const markMyBookCompletedById = (req, res, next) => {
  try{
      return Book.findByIdAndUpdate({_id: req.params.id, userId: req.user.userId}, {$set: { completed: true } })
      .then(() => {
      res.status(200).send({
        "message": "Success"
      })
    }).catch((err)=>{
      console.log('err', err)
      return res.status(400).json({"message": "string"});
    })
  }
  catch(err){
    return res.status(500).json({"message": "string"});
  }
}

module.exports = {
  createBook,
  getBooks,
  getBook,
  updateBook,
  deleteBook,
  getMyBooks,
  updateMyBookById,
  markMyBookCompletedById
};
