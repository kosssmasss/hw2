const fs = require('fs');
const express = require('express');
const morgan = require('morgan');

const app = express();
const mongoose = require('mongoose');

require('dotenv').config();
file = process.env.CERT_FILE;

// mongoose.connect('mongodb+srv://login:password@cluster0.yy9mwgv.mongodb.net/todoapp?retryWrites=true&w=majority');
// mongoose.connect('mongodb+srv://cluster0.5hugzby.mongodb.net/test?authMechanism=MONGODB-X509&authSource=%24external&tls=true&tlsCertificateKeyFile=C%3A%5CUsers%5Ckoss%5CDownloads%5Cmongosh-1.5.4-win32-x64%5Cbin%5CX509-cert-7950903852346685060.pem');
mongoose.connect(`mongodb+srv://cluster0.5hugzby.mongodb.net/test?authMechanism=MONGODB-X509&authSource=%24external&tls=true&tlsCertificateKeyFile=${file}`);

const { filesRouter } = require('./filesRouter.js');
const { booksRouter } = require('./booksRouter.js');
const { usersRouter } = require('./usersRouter.js');
const { authRouter } = require('./authRouter.js');

app.use(express.json());
app.use(morgan('tiny'));

app.use('/api/files', filesRouter);
app.use('/api/notes', booksRouter);
app.use('/api/users', usersRouter);
app.use('/api/auth', authRouter);

const start = async () => {
  try {
    if (!fs.existsSync('files')) {
      fs.mkdirSync('files');
    }
    app.listen(8080);
  } catch (err) {
    console.error(`Error on server startup: ${err.message}`);
  }
};

start();

// ERROR HANDLER
app.use(errorHandler);

function errorHandler(err, req, res, next) {
  console.error(err);
  res.status(500).send({ message: 'Server error' });
}
