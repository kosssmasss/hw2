const mongoose = require('mongoose');

const bookSchema = mongoose.Schema({
  userId: {
    type: mongoose.Schema.Types.ObjectId,
    required: true
  },
  completed: {
    type: Boolean,
    default: false
  },
  text: {
    type: String,
    required: true,
  },
  createdDate: {
    type: Date,
    default: new Date()
  }
});

const Book = mongoose.model('notes', bookSchema);

module.exports = {
  Book,
};
