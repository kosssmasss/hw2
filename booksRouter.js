const express = require('express');
const router = express.Router();
const {
  createBook, getBooks, getBook, deleteBook, updateBook, getMyBooks, updateMyBookById,
  markMyBookCompletedById
} = require('./booksService.js');
const { authMiddleware } = require('./middleware/authMiddleware');

router.post('/', authMiddleware, createBook);

router.get('/', authMiddleware, getBooks);

// router.get('/get-my-books', authMiddleware, getMyBooks);

router.get('/:id', authMiddleware, getBook);

//router.patch('/update-my-book/:id', authMiddleware, updateMyBookById);

router.put('/:id', authMiddleware, updateMyBookById);

router.patch('/:id', authMiddleware, markMyBookCompletedById);

router.delete('/:id', authMiddleware, deleteBook);

module.exports = {
  booksRouter: router,
};
