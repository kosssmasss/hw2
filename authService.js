const { User } = require('./models/Users.js');
// const bcrypt = require('bcrypt');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');

const registerUser = async (req, res, next) => {
  const { name, username, password } = req.body;
  const user = new User({
    name,
    username,
    password: await bcrypt.hash(password, 10),
    // createdDate: new Date()
  });


  user.save()
    // .then(saved => res.json(saved))
    .then(() => res.status(200).json({"message": "Success"}))
    .catch(err => {
      next(err);
    });
}

const loginUser = async (req, res, next) => {
  try{
    const user = await User.findOne({ username: req.body.username });
    if (user && await bcrypt.compare(String(req.body.password), String(user.password))) {
      const payload = { username: user.username, name: user.name, userId: user._id };
      const jwtToken = jwt.sign(payload, 'secret-jwt-key');
      return res.json({
        message: "Success",
        jwt_token: jwtToken
      });
    }
    return res.status(400).json({"message": "string"});
  }
  catch(err){
    return res.status(500).json({"message": "string"})
  }
}

module.exports = {
  registerUser,
  loginUser
};
