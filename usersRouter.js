const express = require('express');
const router = express.Router();
const { meGet, meDelete, mePatch } = require('./usersService.js');
const { authMiddleware } = require('./middleware/authMiddleware');

/* router.post('/register', registerUser);
router.post('/login', loginUser); */

router.get('/me', authMiddleware, meGet);
router.delete('/me', authMiddleware, meDelete);
router.patch('/me', authMiddleware, mePatch);

module.exports = {
  usersRouter: router,
};
