const { User } = require('./models/Users.js');
// const bcrypt = require('bcrypt');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');

// const registerUser = async (req, res, next) => {
//   console.log('req.body req', req.body)
//   const { name, username, password } = req.body;
//   const user = new User({
//     name,
//     username,
//     password: await bcrypt.hash(password, 10)
//   });


//   user.save()
//     .then(saved => res.json(saved))
//     .catch(err => {
//       next(err);
//     });
// }

// const loginUser = async (req, res, next) => {
//   const user = await User.findOne({ username: req.body.username });
//   if (user && await bcrypt.compare(String(req.body.password), String(user.password))) {
//     const payload = { username: user.username, name: user.name, userId: user._id };
//     const jwtToken = jwt.sign(payload, 'secret-jwt-key');
//     return res.json({token: jwtToken});
//   }
//   return res.status(403).json({'message': 'Not authorized'});
// }

const meGet = async (req, res, next) => {
  // console.log('auth req.user', req.user)
  try{
    const userInfo = await User.findOne({ _id: req.user.userId });
    console.log('userInfo', userInfo)
    if(userInfo === null){
      return res.status(400).json({"message": "string"});
    }
    res.status(200).send({
        "user": {
          "_id": userInfo._id,
          "username": userInfo.username,
          "createdDate": userInfo.createdDate
        }
    })
  }
  catch(err){
    return res.status(500).json({"message": "string"});
  }
}

const meDelete = async (req, res, next) => {
  try{
    User.findByIdAndRemove(req.user.userId, (err, doc) => {
      if (!err) {
        res.status(200).send({
          "message": "Success"
        })
      } else {
        return res.status(400).json({"message": "string"});
      }
    })
  }
  catch(err){
    return res.status(500).json({"message": "string"});
  }
}

const mePatch = async (req, res, next) => {
  try{
    console.log('reqreq', req.user.userId)
    const { oldPassword, newPassword } = req.body;
    const user = await User.findOne({ _id: req.user.userId });
    console.log('user', user)
    if (user && await bcrypt.compare(String(oldPassword), String(user.password))) {
      user.password = await bcrypt.hash(newPassword, 10)
      user.save()
      return res.json({
        message: "Success"
      })
    }
    //else
    return res.json({
      message: "string"
    })
  }
  catch(err){
    return res.status(500).json({"message": "string"});
  }
}

module.exports = {
  meGet,
  meDelete,
  mePatch
};
